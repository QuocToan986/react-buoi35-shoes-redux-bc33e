import React, { Component } from "react";
import { connect } from "react-redux";
import CSS from "./ex_Shoe_Redux.module.css";

class ItemShoe extends Component {
  containerGrid = {
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr",
    gap: "20px",
  };

  renderContent = () => {
    // console.log(this.props.listShoes);
    return this.props.listShoes.map((item, index) => {
      return (
        <div
          id={CSS.cart_item}
          className="card"
          style={{ width: "100%" }}
          key={index}
        >
          <img className="card-img-top" src={item.image} alt="Card image cap" />
          <i
            id={CSS.eye_detail}
            className="fa fa-info-circle"
            data-toggle="modal"
            data-target="#exampleModalCenter"
          />
          <div className="card-body">
            <h5 className="card-title">{item.name}</h5>
            <a href="#" className="btn btn-primary w-100">
              Add to cart
            </a>
          </div>
          {/* start modal */}
          <div>
            <div
              className="modal fade"
              id="exampleModalCenter"
              tabIndex={-1}
              role="dialog"
              aria-labelledby="exampleModalCenterTitle"
              aria-hidden="true"
            >
              <div
                className="modal-dialog modal-dialog-centered"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">
                      Detail
                    </h5>
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body row">
                    <div className="col-5">
                      <img style={{ width: "100%" }} src={item.image} alt="" />
                    </div>
                    <div className="col-7 text-left">
                      <h4 style={{ marginBottom: 16 }}>{item.name}</h4>
                      <p style={{ fontSize: 14, marginBottom: 8 }}>
                        {item.shortDescription}
                      </p>
                      <p style={{ fontSize: 14 }}>{item.description}</p>
                      <p
                        style={{
                          marginBottom: 0,
                          textAlign: "right",
                          fontSize: 20,
                          color: "red",
                          fontWeight: "bolder",
                          marginRight: "5px",
                        }}
                      >
                        Price: {item.price}$
                      </p>
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="button" className="btn btn-primary">
                      Add to cart
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };
  render() {
    console.log(this.props.listShoes[0]);
    return <div style={this.containerGrid}>{this.renderContent()}</div>;
  }
}
let mapToPropsState = (state) => {
  return {
    listShoes: state.numberReducer.shoes,
  };
};
export default connect(mapToPropsState)(ItemShoe);
